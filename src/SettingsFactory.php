<?php

namespace Drupal\client_config_care;

use Drupal\Core\Site\Settings;

class SettingsFactory {

  public function create(): SettingsModel {
    $settingsArray = Settings::get('client_config_care');
    if (isset($settingsArray['deactivated'])) {
      return new SettingsModel($settingsArray['deactivated']);
    }

    return new SettingsModel();
  }

}
